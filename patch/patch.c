#include <stdio.h> 
#include <stdlib.h> 
#include <string.h>
#include <errno.h>

#define MAX(a,b) a > b ? a : b

int lncount(char *file);
void linesep(char **argv);
int displayError();

char **x, **y;
int nol1, nol2;

int main(int argc, char *argv[]){
	FILE *fp1;
	FILE *fp2;
	int lnf1[2],lnf[2];
	int i;
	int add_flag = 0, del_flag = 0, change_flag = 0;

	char *file[2];
	file[0] = argv[1];
	file[1] = argv[2];

	nol1 = lncount(file[0]);
	nol2 = lncount(file[1]);

   	linesep(argv);
   	fp1 = fopen(argv[1], "w");
   	if(fp1 == NULL) {
		displayError();
		return -1;
	}
	fp2 = fopen(argv[2], "r");
	if(fp1 == NULL) {
		displayError();
		return -1;
	}
	printf("Patching file %s\n", argv[1]);
   	int curlinep = 0, curlineo = 0;
	while(curlinep < 1) {
		char curchar;
		char *copy = strdup(y[curlinep]);
		i = 0;
		//this while finds which operation is to be performed
		while(1) {
			curchar = copy[i++];
			switch(curchar) {
				case '0': case '1': case '2':
				case '4': case '5': case '6':
				case '7': case '8': case '9':	
				case '3':
					break;

				case ',':
					break;

				case 'a':
					add_flag = 1;
					break;

				case 'd':
					del_flag = 1;
					break;

				case 'c':
					change_flag = 1;
					break;

				default:
					break;
			}
			if(curchar == '\n') {
				break;
			}
		}

		if(add_flag) {
			enum state {NUM, DIG, END, ERR};
			int curstate = NUM;
			int nextstate;
			int num1 = 0, num2 = 0, num3 = 0, num4 = 0, sum = 0;
			int i = 0;
			char *copy = strdup(y[curlinep++]);
			// this while gets the line numbers
			while(1) {
				curchar = copy[i++];
				printf("%c", curchar);
				switch(curstate) {
					case NUM:
						switch(curchar) {
							case '0': case '1': case '2':
							case '4': case '5': case '6':
							case '7': case '8': case '9':
							case '3': 
								nextstate = DIG;
								sum = curchar - '0';
								break;

							default:
								nextstate = ERR;
								break;
						}
						break;
					case DIG:
						switch(curchar) {
							case '0': case '1': case '2':
							case '4': case '5': case '6':
							case '7': case '8': case '9':
							case '3': 
								nextstate = DIG;
								sum = sum * 10 + curchar - '0';
								break;

							case 'a':
								num1 = sum;
								nextstate = NUM;
								break;

							case ',':
								nextstate = NUM;
								num3 = sum;
								break;

							case '\n':
								nextstate = END;
								num4 = sum;
								curstate = nextstate;	
								break;

							default:
								nextstate = ERR;	
								break;
						}
						break;

					case END:
						curchar = copy[i++];
						break;

					default:
						break;
				}
				curstate = nextstate;
				if(curchar == '\n') {
					break;
				}
			}

			for(int m = curlineo; m < num1; m++) {
				fprintf(fp1, "%s", x[m]);
				curlineo++;
			}

			if(num4 == 0) {
				fprintf(fp1, "%s", y[curlinep] + 2);
				curlinep++;
			}
			else {
				for (int m = num3; m <= num4; m++) {
					fprintf(fp1, "%s", y[curlinep] + 2);
					curlinep++;
				}
			}

			if(nol1 > num1) {
				for (int m = num1; m < nol1; m++) {
					fprintf(fp1, "%s", x[m]);
					curlineo++;
				}
			}
		}

		if(change_flag) {
			enum state {NUM, DIG, END, ERR};
			int curstate = NUM;
			int nextstate;
			int num1 = 0, num2 = 0, num3 = 0, num4 = 0, sum = 0;
			int i = 0;
			char *copy = strdup(y[curlinep++]);
			// this while gets the line numbers
			while(1) {
				curchar = copy[i++];  
				switch(curstate) {
					case NUM:
						switch(curchar) {
							case '0': case '1': case '2':
							case '4': case '5': case '6':
							case '7': case '8': case '9':
							case '3': 
								nextstate = DIG;
								sum = curchar - '0';
								break;

							default:
								nextstate = ERR;
								break;
						}
						break;
					case DIG:
						switch(curchar) {
							case '0': case '1': case '2':
							case '4': case '5': case '6':
							case '7': case '8': case '9':
							case '3': 
								nextstate = DIG;
								sum = sum * 10 + curchar - '0';
								break;

							case 'c': 
								if(num1)
									num2 = sum;
								else
									num1 = sum;

								nextstate = NUM;
								break;

							case ',':
								nextstate = NUM;
								if(num1)
									num3 = sum;
								else
									num1 = sum;
								break;

							case '\n':
								nextstate = END;
								if(num3)
									num4 = sum;
								else
									num3 = sum;
								curstate = nextstate;	
								break;

							default:
								nextstate = ERR;	
								break;
						}
						break;

					case END:
						curchar = copy[i++];
						break;

					default:
						break;
				}
				curstate = nextstate;
				if(curchar == '\n') {
					break;
				}
			}

			//skip lines in patchfile
			if(num4)
				curlinep = curlinep + (num4 - num3 + 2);
			else 
				curlinep = curlinep + 2;

			if((num2 == 0 && num4 != 0) || (num2 != 0 && num4 == 0)) {
				printf("Hunk FAILED %d\n", curlinep);
				break;
			}

			for(int m = curlineo; m < num1 - 1; m++) {
				fprintf(fp1, "%s", x[m]);
				curlineo++;
			}

			if(num4 == 0) {
				fprintf(fp1, "%s", y[curlinep] + 2);
				curlinep++;
			}
			else {
				for (int m = num3; m <= num4; m++) {
					fprintf(fp1, "%s", y[curlinep] + 2);
					curlinep++;
				}
			}

			if(nol1 > MAX(num1,num2)) {
				if(num2)
					curlineo = curlineo + abs(num2 - num1 + 1);
				else
					curlineo++;

				for (int m = MAX(num1,num2); m < nol1; m++) {
					fprintf(fp1, "%s", x[curlineo]);
					curlineo++;
				}
			}
		}

		if(del_flag) {
			enum state {NUM, DIG, END, ERR};
			int curstate = NUM;
			int nextstate;
			int num1 = 0, num2 = 0, num3 = 0, num4 = 0, sum = 0;
			int i = 0;
			char *copy = strdup(y[curlinep++]);
			// this while gets the line numbers
			while(1) {
				curchar = copy[i++];
				switch(curstate) {
					case NUM:
						switch(curchar) {
							case '0': case '1': case '2':
							case '4': case '5': case '6':
							case '7': case '8': case '9':
							case '3': 
								nextstate = DIG;
								sum = curchar - '0';
								break;

							default:
								nextstate = ERR;
								break;
						}
						break;
					case DIG:
						switch(curchar) {
							case '0': case '1': case '2':
							case '4': case '5': case '6':
							case '7': case '8': case '9':
							case '3': 
								nextstate = DIG;
								sum = sum * 10 + curchar - '0';
								break;

							case 'd':
								num2 = sum;
								nextstate = NUM;
								break;

							case ',':
								nextstate = NUM;
								num1 = sum;
								break;

							case '\n':
								nextstate = END;
								num3 = sum;
								curstate = nextstate;	
								break;

							default:
								nextstate = ERR;	
								break;
						}
						break;

					case END:
						curchar = copy[i++];
						break;

					default:
						break;
				}
				curstate = nextstate;
				if(curchar == '\n') {
					break;
				}
			}

			if(nol1 > num2) {
					printf("Previous patch detected\n");
					break;
				}

			for(int m = curlineo; m < num3; m++) {
				fprintf(fp1, "%s", x[m]);
				curlineo++;
			}
		}

	}
	fclose(fp1);
    fclose(fp2);

    for(i = 0; i < nol1; i++) {
		free(x[i]);
	}
	for(i = 0; i < nol2; i++) {
		free(y[i]);
	}
	free(x);
	free(y);
  	return 0;
}

void linesep(char **argv) {
	FILE *fp1 = NULL;
	FILE *fp2 = NULL;
	ssize_t linelen1, linelen2;
	size_t len1, len2;
	char *ln1 = NULL; 
	char *ln2 = NULL;
	int noc = 512;
	int i = 0;
	int j = 0;

	fp1 = fopen(argv[1], "r");
	if(fp1 == NULL) {
		displayError();
	}	
	fp2 = fopen(argv[2], "r");
	if(fp2 == NULL) {
		displayError();
	}
	
	x = (char **)malloc(nol1 * sizeof(char *));
	y = (char **)malloc(nol2 * sizeof(char *));

	while((linelen1 = getline(&ln1, &len1, fp1)) != -1) {
		x[i] = (char *)malloc(noc * sizeof(char));
		x[i] = strdup(ln1);
		i++;
	}

	while((linelen2 = getline(&ln2, &len2, fp2)) != -1) {
		y[j] = (char *)malloc(noc * sizeof(char));
		y[j] = strdup(ln2);
		j++;
	}

	fclose(fp1);
	fclose(fp2);

}

int lncount(char file[]) {
	FILE *fp = NULL;
	char *line = NULL;
    size_t linelen = 0;
	ssize_t check;
	int count = 0;

	fp = fopen(file, "r");
	while ((check = getline(&line, &linelen, fp)) > 0)
		count++;

	fclose(fp);
	return count;
}

int displayError() {
	perror("Error :");
	return errno;
}
