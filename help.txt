Book : Introduction to Algorithms
	 - Charles E. Leiserson, Clifford Stein,
		Ronald Rivest, and Thomas H. Corm 

Links :
1. https://github.com/ShreyanshG22/diff-miniproject/blob/master/diff.c
2. https://stackoverflow.com/questions/14020380/strcpy-vs-strdup
3 .https://bytes.com/topic/c/answers/843278-not-able-locate-line_max-limits-h
4. https://stackoverflow.com/questions/729692/why-should-text-files-end-with-a-newline
5. http://wordaligned.org/articles/longest-common-subsequence
6. https://stackoverflow.com/questions/805626/diff-algorithm
7. http://www.dreamincode.net/forums/topic/146250-how-to-use-getline-in-c-programming/
8. https://linuxacademy.com/blog/linux/introduction-using-diff-and-patch/

Other : Various man pages.